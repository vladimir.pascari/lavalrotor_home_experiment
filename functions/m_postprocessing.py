"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    
    abs_value = np.array([])
    for i in range(0, len(x)):
        abs_value= np.append(abs_value, ((x[i]**2+y[i]**2+z[i]**2)**0.5))
    return(abs_value)

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    n = len(time)
    
    data_int = np.array([])
    
    time_int = np.linspace(time[0], time[-1], num = len(time))
    data_int = np.append(data_int, data[0])
    
    for i in range(1,n-2):
        idx = np.abs(time - time_int[i]).argmin()
        if time_int[idx]>=time[idx]:
            a = idx
            b = idx+1
        else:
            a = idx-1
            b = idx
            
        
        m = (data[b] - data[a])/(time[b]-time[a])

        k = data[b] - m*time[b]
        
        acc_int = m*time_int[i] + k
        data_int = np.append(data_int, acc_int)
    
    data_int = np.append(data_int, data[n-1])
    
    return([time_int, data_int])

    

     


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    
    X = fft(x)
    N = len(X)
    
    
    sr = len(time)/time[-1]
    
    fstep = sr/N
    freq = np.linspace(0, (N-1)*fstep, N)
    
    return(X, freq)
    
    
